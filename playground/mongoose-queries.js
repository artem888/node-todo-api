const {ObjectId} = require('mongodb');

const {mongoose} = require('./../server/db/mongoose');
const {Todo} = require('./../server/models/todo');
const {User} = require('./../server/models/user');

var userId = '5ba6317c9b52e94207be1e4a';

User.findById(userId).then((user) => {
  if (!user) {
    return console.log('user not found');
  }

  console.log('User ', user);
}).catch((e) => console.log(e));

// var id = '5bafa01e53aa1d07b05efcb4111';
//
// if (!ObjectId.isValid(id)) {
//   console.log('id not valid');
// }

// Todo.find({_id: id}).then((todos) => {
//   console.log('Todos: ', todos);
// });
//
// Todo.findOne({_id: id}).then((todo) => {
//   console.log('Todos: ', todo);
// });

// Todo.findById(id).then((todo) => {
//   if (!todo) {
//     return console.log('todo not found');
//   }
//   console.log('Todos: ', todo);
// }).catch((e) => console.log(e));