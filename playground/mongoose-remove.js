const {ObjectId} = require('mongodb');

const {mongoose} = require('./../server/db/mongoose');
const {Todo} = require('./../server/models/todo');
const {User} = require('./../server/models/user');

// Todo.remove({}).then((result) => {
//   console.log(result);
// });

// Todo.findAndRemove()
// Todo.findByIdAndRemove()

Todo.findByIdAndRemove('5bd0e2ef4b6ce53c6e1eed31').then((todo) => {
  console.log(todo);
});